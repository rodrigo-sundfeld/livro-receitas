# Livro de receitas :cake:

### Olá! Bem vindo ao meu livro de receitas :fork_and_knife:

- Lasanha ao molho gorgonzola
- Hambúrguer artesanal com cheddar e cebola ao shoyu :hamburger:
- Torta de abobrinha
