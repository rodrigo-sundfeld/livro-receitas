# Lasanha ao molho gorgonzola  :smile:



### Ingredientes

- 500 g de massa fresca para lasanha
- 400 g de peito de peru defumado (fatiado)
- 400 g de mussarela (fatiada)
- 150 g de queijo gorgonzola
- 1 lata de creme de leite (sem soro)
- 300 ml de leite
- 1 colher de sopa de farinha de trigo
- 1 taça de vinho branco seco
- 250 g de ricota fresca
- Queijo parmesão a gosto
- Tempero a gosto



### MODO DE PREPARO

1. Coloque numa panela o leite frio com a farinha de trigo já dissolvida e tempere a gosto (geralmente sal, alho, pimenta do reino e noz moscada) e mexa até engrossar.
2. Acrescente o creme de leite, o queijo gorgonzola e a ricota (amassada com um garfo).
3. Acrescente o vinho para "desandar" o queijo e reserve.
4. Monte a lasanha intercalando a massa, a mussarela e o peito de peru fatiado.
5. Lembre-se de colocar antes uma camada do molho no fundo do refratário e entre as camadas de massa.
6. Cubra com o molho, salpique parmesão e leve ao forno pré-aquecido a 200° graus até borbulhar (mais ou menos 30 minutos).

